# Shadowcker

## Introduction

This is updated docker work based on Camille work to run Shadow in docker.
This only work with Intel integrated GPU for the moment.

## Requirements

 - Docker
 - Docker Compose
 - Compatible GPU with DRI exposed & rights to access it

## Build & Run

```bash
git clone https://gitlab.com/aar642/shadowcker.git
cd shadowcker
make beta
make start
```

## Discord servers

  Find us on Shadow Official Discord servers!

- [Discord Shadow FR](https://discordapp.com/invite/shadowtech)
- [Discord Shadow UK](https://discordapp.com/invite/ShadowEN)
- [Discord Shadow DE](https://discord.gg/shadowde)
- [Discord Shadow US](https://shdw.me/USDiscord)
- [Discord Shadow Community Projects](https://discord.gg/9HwHnHq)

## Maintainers
![Alex^#1629](https://cdn.discordapp.com/avatars/401575828590428161/36d0ac43c2cb3a72d41c51b0c8375f65.png?size=64 "Alex^#1629")
![Drixs#6784](https://cdn.discordapp.com/avatars/257232817648304128/a342603d17dbb8b218735326df30917f.png?size=64 "Drixs#6784")
![Le Panzer de Rodin#9477](https://cdn.discordapp.com/avatars/195609740271681537/c017658ad6b1fe0715d8b7acef8bac99.png?size=64 "Le Panzer de Rodin#9477")

## Disclaimer

This is a community project, project is not affliated to Blade in any way.

[Shadow](https://shadow.tech) logo and embeded Linux client is property of [Blade Group](http://www.blade-group.com/).
