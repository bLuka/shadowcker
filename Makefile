AVAILABLE_FOLDERS=$(wildcard */Dockerfile)
RULES=$(AVAILABLE_FOLDERS:/Dockerfile=)

IMAGE_NAME=shadow-beta
BUILD_ARGS=--force-rm --build-arg VIDEO_GID=$$(cat /etc/group | grep video | cut -d: -f3)

run: beta start

start:
	docker-compose up

${RULES}:
	docker build -t ${IMAGE_NAME} ${BUILD_ARGS} $@

clean:
	docker image prune

fclean:
	docker-compose rm
	docker image rm shadow-beta || true
	$(MAKE) clean

.PHONY: ${RULES}
